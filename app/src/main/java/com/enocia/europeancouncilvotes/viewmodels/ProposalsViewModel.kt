package com.enocia.europeancouncilvotes.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.ProposalWithCompleteVotes
import com.enocia.europeancouncilvotes.repositories.ProposalsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

typealias ProposalClickCallback = (Proposal) -> Unit
typealias NullableProposalClickCallback = ((Proposal) -> Unit)?

@HiltViewModel
class ProposalsViewModel @Inject constructor(application: Application, private val repository: ProposalsRepository) : AndroidViewModel(application) {
    lateinit var onProposalClicked: ProposalClickCallback
        private set

    private val _proposals: MutableLiveData<List<Proposal>> = MutableLiveData(ArrayList())
    val proposals: LiveData<List<Proposal>> = Transformations.map(_proposals) {
        Proposal.refreshDateFormatter()
        it
    }

    private val _selectedProposal: MutableLiveData<ProposalWithCompleteVotes?> =
        MutableLiveData(null)
    val selectedProposal: LiveData<ProposalWithCompleteVotes?> =
        Transformations.map(_selectedProposal) {
            Proposal.refreshDateFormatter()
            it
        }

    private var loadingState = ProposalsLoadingState(_proposals)
    val shouldDisplayFullLoadingView: LiveData<Boolean> =
        Transformations.map(loadingState.shouldDisplayFullLoadingView) { it }
    val shouldDisplayLoadingProgressBarOnly: LiveData<Boolean> =
        Transformations.map(loadingState.shouldDisplayLoadingProgressBarOnly) { it }

    fun loadDataIfNeeded() {
        if (_proposals.value.isNullOrEmpty()) {
            loadData()
        }
    }

    fun loadData(forceAPIQuery: Boolean = false) {
        viewModelScope.launch {
            loadingState.startLoading()
            if (loadingState.loadingJob?.isActive == true) {
                loadingState.loadingJob?.cancel()
            }
            val job: Job = loadDataAsync(forceAPIQuery)
            loadingState.loadingJob = job
            job.join()
        }
    }

    private suspend fun loadDataAsync(forceAPIQuery: Boolean) =
        viewModelScope.async(Dispatchers.Default, CoroutineStart.LAZY) {
            try {
                val newProposals: List<Proposal> = repository.getProposals(forceAPIQuery)
                _proposals.postValue(newProposals)
                initializeOrRefreshSelectedProposal(newProposals)
            } catch (e: Exception) {
                throw e
            } finally {
                withContext(Dispatchers.Main) { loadingState.stopLoading() }
            }
        }

    private fun initializeOrRefreshSelectedProposal(newProposals: List<Proposal>) {
        try {
            val current = _selectedProposal.value

            val refreshedProposal: Proposal = if (current == null) {
                newProposals.first()
            } else {
                newProposals.single {
                    current.proposal.identifier == it.identifier
                }
            }

            selectProposal(refreshedProposal)
        } catch (e: Exception) {
            Log.w(null, "The selected proposal was not found in the new list of proposals.", e)
        }
    }

    private fun selectProposal(proposal: Proposal) {
        viewModelScope.launch {
            try {
                val newSelectedProposal: Deferred<ProposalWithCompleteVotes> =
                    async(Dispatchers.IO) { repository.getDetailedProposal(proposal) }
                _selectedProposal.value = newSelectedProposal.await()
            } catch (e: Exception) {
                throw e
            }
        }
    }

    fun setOnProposalClick(onProposalClick: NullableProposalClickCallback) {
        onProposalClicked = {
            selectProposal(it)
            onProposalClick?.invoke(it)
        }
    }
}