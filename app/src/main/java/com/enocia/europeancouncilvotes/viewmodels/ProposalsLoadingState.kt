package com.enocia.europeancouncilvotes.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.enocia.europeancouncilvotes.model.Proposal
import kotlinx.coroutines.Job

class ProposalsLoadingState(private val proposals: LiveData<List<Proposal>>) {
    private val isLoadingData = MutableLiveData(false)
    var loadingJob: Job? = null

    val shouldDisplayFullLoadingView: MediatorLiveData<Boolean> by lazy {
        val liveData = MediatorLiveData<Boolean>()
        liveData.value = shouldDisplayFullLoadingView()
        liveData.addSource(isLoadingData) { liveData.value = shouldDisplayFullLoadingView() }
        liveData.addSource(proposals) { liveData.value = shouldDisplayFullLoadingView() }
        liveData
    }

    val shouldDisplayLoadingProgressBarOnly: MediatorLiveData<Boolean> by lazy {
        val liveData = MediatorLiveData<Boolean>()
        liveData.value = shouldDisplayLoadingProgressBarOnly()
        liveData.addSource(isLoadingData) {
            liveData.value = shouldDisplayLoadingProgressBarOnly()
        }
        liveData.addSource(proposals) { liveData.value = shouldDisplayLoadingProgressBarOnly() }
        liveData
    }

    private fun shouldDisplayFullLoadingView(): Boolean {
        return (isLoadingData.value ?: false && proposals.value.isNullOrEmpty())
    }

    private fun shouldDisplayLoadingProgressBarOnly(): Boolean {
        return (isLoadingData.value ?: false && !proposals.value.isNullOrEmpty())
    }

    fun startLoading() {
        isLoadingData.value = true
    }

    fun stopLoading() {
        isLoadingData.value = false
    }
}