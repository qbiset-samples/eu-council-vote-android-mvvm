package com.enocia.europeancouncilvotes

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EuVotesApplication: Application()