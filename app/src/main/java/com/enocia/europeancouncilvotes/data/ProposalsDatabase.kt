package com.enocia.europeancouncilvotes.data

import android.content.Context
import androidx.room.*
import com.enocia.europeancouncilvotes.model.Country
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.Vote
import java.util.*

@Database(entities = [Proposal::class, Country::class, Vote::class], version = 1)
@TypeConverters(DatabaseConverters::class)
abstract class ProposalsDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao
    abstract fun proposalDao(): ProposalDao
    abstract fun voteDao(): VoteDao
}

class DatabaseConverters {
    @TypeConverter
    fun dateFromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}

object ProposalsDatabaseFactory {
    fun createProposalsDatabase(appContext: Context): ProposalsDatabase = Room.databaseBuilder(
        appContext,
        ProposalsDatabase::class.java,
        "proposals-database"
    ).build()
}