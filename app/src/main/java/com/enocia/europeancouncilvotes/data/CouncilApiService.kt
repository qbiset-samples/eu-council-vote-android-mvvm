package com.enocia.europeancouncilvotes.data

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET

interface CouncilApiService {
    @GET("country")
    suspend fun getCountries(): String

    @GET("document")
    suspend fun getProposals(): String
}

object CouncilApiFactory {
    private const val BASE_URL = "http://api.epdb.eu/council/"

    fun createCouncilApiService(): CouncilApiService {
        val retrofit = createRetrofit()
        return retrofit.create(CouncilApiService::class.java)
    }

    private fun createRetrofit(): Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()
}