package com.enocia.europeancouncilvotes.data

import androidx.room.*
import com.enocia.europeancouncilvotes.model.Country
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.ProposalWithCompleteVotes
import com.enocia.europeancouncilvotes.model.Vote

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(objects: List<T>): List<Long>
}

@Dao
abstract class CountryDao : BaseDao<Country> {
    @Query("SELECT * FROM countries ORDER BY name")
    abstract suspend fun loadAllCountries(): List<Country>
}

@Dao
abstract class ProposalDao : BaseDao<Proposal> {
    @Query("SELECT * FROM proposals ORDER BY date, identifier")
    abstract suspend fun loadAllProposals(): List<Proposal>

    @Transaction
    @Query("SELECT * FROM proposals WHERE identifier = :proposalIdentifier ORDER BY date, identifier")
    abstract suspend fun loadProposalWithCompleteVotes(proposalIdentifier: String): ProposalWithCompleteVotes

    @Query("SELECT COUNT(*) FROM proposals")
    abstract suspend fun count(): Long
}

@Dao
abstract class VoteDao : BaseDao<Vote>