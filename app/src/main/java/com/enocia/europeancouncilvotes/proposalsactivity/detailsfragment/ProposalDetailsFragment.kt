package com.enocia.europeancouncilvotes.proposalsactivity.detailsfragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.databinding.ProposalDetailsFragmentBinding
import com.enocia.europeancouncilvotes.viewmodels.ProposalsViewModel

class ProposalDetailsFragment : Fragment(R.layout.proposal_details_fragment) {

    // Scoped to the lifecycle of the fragment's view (between onViewCreated and onDestroyView)
    private var binding: ProposalDetailsFragmentBinding? = null
    private val viewModel: ProposalsViewModel by activityViewModels()

    companion object {
        fun newInstance() = ProposalDetailsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = ProposalDetailsFragmentBinding.bind(view)
        this.binding = binding

        val recyclerViewAdapter = VotesListAdapter(viewModel.selectedProposal.value)
        binding.proposalDetailsRecyclerview.adapter = recyclerViewAdapter

        viewModel.selectedProposal.observe(viewLifecycleOwner) {
            recyclerViewAdapter.setNewData(viewModel.selectedProposal.value)
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

}