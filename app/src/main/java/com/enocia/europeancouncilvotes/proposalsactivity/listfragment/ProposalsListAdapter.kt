package com.enocia.europeancouncilvotes.proposalsactivity.listfragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.enocia.europeancouncilvotes.databinding.ProposalItemViewBinding
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.viewmodels.ProposalClickCallback

class ProposalsListAdapter(private val onProposalClicked: ProposalClickCallback) :
    ListAdapter<Proposal, ProposalsListAdapter.ProposalViewHolder>(ProposalDiffCallback()) {

    class ProposalViewHolder private constructor(
        private val binding: ProposalItemViewBinding,
        private val onProposalClicked: (Proposal) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun updateContent(proposal: Proposal) {
            binding.proposalItemDateTextview.text = proposal.formattedDate
            binding.proposalItemTitleTextview.text = proposal.title
        }

        fun updateClickListener(proposal: Proposal) {
            itemView.setOnClickListener {
                onProposalClicked(proposal)
            }
        }

        companion object {
            fun from(parent: ViewGroup, onItemClicked: ProposalClickCallback): ProposalViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ProposalItemViewBinding.inflate(layoutInflater, parent, false)

                return ProposalViewHolder(binding, onItemClicked)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProposalViewHolder {
        return ProposalViewHolder.from(parent, onProposalClicked)
    }

    override fun onBindViewHolder(holder: ProposalViewHolder, position: Int) {
        val proposal: Proposal = getItem(position)
        holder.updateContent(proposal)
        holder.updateClickListener(proposal)
    }

}

private class ProposalDiffCallback : DiffUtil.ItemCallback<Proposal>() {
    override fun areItemsTheSame(oldItem: Proposal, newItem: Proposal): Boolean {
        return oldItem.identifier == newItem.identifier
    }

    override fun areContentsTheSame(oldItem: Proposal, newItem: Proposal): Boolean {
        return oldItem == newItem
    }
}