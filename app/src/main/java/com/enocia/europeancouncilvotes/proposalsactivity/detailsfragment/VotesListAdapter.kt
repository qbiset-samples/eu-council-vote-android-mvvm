package com.enocia.europeancouncilvotes.proposalsactivity.detailsfragment

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.enocia.europeancouncilvotes.model.ProposalWithCompleteVotes

class VotesListAdapter(proposal: ProposalWithCompleteVotes?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var fullProposal: ProposalWithCompleteVotes? = null

    init {
        setNewData(proposal, true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ProposalDetailsViewHolder.VIEW_TYPE) {
            ProposalDetailsViewHolder.from(parent)
        } else {
            VoteViewHolder.from(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        fullProposal?.let {
            when (holder) {
                is ProposalDetailsViewHolder -> {
                    holder.updateContent(it.proposal)
                }
                is VoteViewHolder -> {
                    holder.updateContent(it.votes[position - 1])
                }
            }
        }
    }

    override fun getItemCount(): Int {
        var count = fullProposal?.votes?.count() ?: 0
        if (count > 0) { count += 1 }
        return count
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            ProposalDetailsViewHolder.VIEW_TYPE
        } else {
            VoteViewHolder.VIEW_TYPE
        }
    }

    fun setNewData(proposal: ProposalWithCompleteVotes?, notifyObservers: Boolean = true) {
        fullProposal = proposal
        if (notifyObservers) {
            notifyDataSetChanged()
        }
    }

}