package com.enocia.europeancouncilvotes.proposalsactivity.detailsfragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.enocia.europeancouncilvotes.databinding.ProposalDetailsItemViewBinding
import com.enocia.europeancouncilvotes.databinding.VoteItemViewBinding
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.VoteAndCountry

sealed class VotesListViewHolder(view: View) : RecyclerView.ViewHolder(view)

class ProposalDetailsViewHolder(private val binding: ProposalDetailsItemViewBinding) :
    VotesListViewHolder(binding.root) {

    fun updateContent(proposal: Proposal) {
        binding.proposalDetailsDateTextView.text = proposal.formattedDate
        binding.proposalDetailsTitleTextView.text = proposal.title
    }

    companion object {
        const val VIEW_TYPE: Int = 1

        fun from(parent: ViewGroup): ProposalDetailsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ProposalDetailsItemViewBinding.inflate(layoutInflater, parent, false)

            return ProposalDetailsViewHolder(binding)
        }
    }
}

class VoteViewHolder(private val binding: VoteItemViewBinding) : VotesListViewHolder(binding.root) {

    fun updateContent(vote: VoteAndCountry) {
        binding.voteItemCountryTextview.text = vote.country.name
        binding.voteItemValueTextview.text = vote.vote.value
    }

    companion object {
        const val VIEW_TYPE: Int = 2

        fun from(parent: ViewGroup): VoteViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = VoteItemViewBinding.inflate(layoutInflater, parent, false)

            return VoteViewHolder(binding)
        }
    }
}