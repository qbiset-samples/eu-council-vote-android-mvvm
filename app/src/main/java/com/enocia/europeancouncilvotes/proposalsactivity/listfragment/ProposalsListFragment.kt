package com.enocia.europeancouncilvotes.proposalsactivity.listfragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.databinding.ProposalsListFragmentBinding
import com.enocia.europeancouncilvotes.viewmodels.ProposalsViewModel

class ProposalsListFragment : Fragment(R.layout.proposals_list_fragment) {

    // Scoped to the lifecycle of the fragment's view (between onViewCreated and onDestroyView)
    private var binding: ProposalsListFragmentBinding? = null
    private val viewModel: ProposalsViewModel by activityViewModels()

    companion object {
        fun newInstance() = ProposalsListFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = ProposalsListFragmentBinding.bind(view)
        this.binding = binding

        val listAdapter = ProposalsListAdapter(viewModel.onProposalClicked)
        binding.proposalsListRecyclerview.adapter = listAdapter

        viewModel.proposals.observe(viewLifecycleOwner) {
            listAdapter.submitList(it)
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

}