package com.enocia.europeancouncilvotes.proposalsactivity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.databinding.ProposalsActivityBinding
import com.enocia.europeancouncilvotes.proposalsactivity.detailsfragment.ProposalDetailsFragment
import com.enocia.europeancouncilvotes.proposalsactivity.listfragment.ProposalsListFragment
import com.enocia.europeancouncilvotes.viewmodels.NullableProposalClickCallback
import com.enocia.europeancouncilvotes.viewmodels.ProposalsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * The main and only activity of the app.
 *
 * This activity has two possible layouts:
 * + The default vertical layout is comprised of a single full-screen container initialized with [ProposalsListFragment]. When the user taps a proposal in the list, the content of the container is replaced by a [ProposalDetailsFragment] to show the full voting records of the selected proposal. A back button also appears to allow the user to go back to [ProposalsListFragment].
 * + The wide layout is comprised of two containers, allowing [ProposalsListFragment] and [ProposalDetailsFragment] to be displayed side by side.
 *
 * A refresh button in the app bar also allows the user to force the app to query the web API for up-to-date data.
 *
 * @see [ProposalsViewModel]
 * @author Quentin BISET
 */
@AndroidEntryPoint
class ProposalsActivity : AppCompatActivity() {

    private val viewModel: ProposalsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Data binding and initial data fetch
        val binding: ProposalsActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.proposals_activity)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val isDualPanel: Boolean = (binding.secondaryFragmentContainer != null)
        setupViewModel(viewModel, isDualPanel)
        // If the activity is switching to a dual panel layout when the user was viewing a proposal's details fragment,
        // we must roll the main container back to the proposals list fragment and remove the back button from the app bar.
        if (isDualPanel && supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> { // The home button should only appear when the Details fragment is being displayed
                bringListFragmentBack()
                true
            }

            R.id.refresh -> {
                viewModel.loadData(true)
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun setupViewModel(viewModel: ProposalsViewModel, viewIsDualPanel: Boolean) {
        val callback: NullableProposalClickCallback = if (viewIsDualPanel) {
            null
        } else {
            { replaceListFragmentWithDetailsFragment() }
        }

        viewModel.setOnProposalClick(callback)
        viewModel.loadDataIfNeeded()
    }

    private fun replaceListFragmentWithDetailsFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val newFragment = ProposalDetailsFragment.newInstance()
        transaction.replace(R.id.main_fragment_container, newFragment, "proposal_details")
        transaction.addToBackStack(null)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        transaction.commit()
    }

    private fun bringListFragmentBack() {
        onBackPressed()
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

}