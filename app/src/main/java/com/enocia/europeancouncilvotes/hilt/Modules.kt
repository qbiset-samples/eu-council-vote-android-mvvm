package com.enocia.europeancouncilvotes.hilt

import android.content.Context
import com.enocia.europeancouncilvotes.data.CouncilApiFactory
import com.enocia.europeancouncilvotes.data.CouncilApiService
import com.enocia.europeancouncilvotes.data.ProposalsDatabase
import com.enocia.europeancouncilvotes.data.ProposalsDatabaseFactory
import com.enocia.europeancouncilvotes.repositories.ProposalsRepository
import com.enocia.europeancouncilvotes.repositories.ProposalsRoomDataSource
import com.enocia.europeancouncilvotes.repositories.ProposalsWebDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideProposalsDatabase(@ApplicationContext appContext: Context): ProposalsDatabase = ProposalsDatabaseFactory.createProposalsDatabase(appContext)
}

@Module
@InstallIn(SingletonComponent::class)
object CouncilWebApiModule {
    @Provides
    @Singleton
    fun provideCouncilWebApiService(): CouncilApiService = CouncilApiFactory.createCouncilApiService()
}

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {
    @Provides
    fun createProposalsRepository(roomDataSource: ProposalsRoomDataSource, webDataSource: ProposalsWebDataSource): ProposalsRepository = ProposalsRepository(roomDataSource, webDataSource)

    @Provides
    fun createProposalsRoomDataSource(database: ProposalsDatabase): ProposalsRoomDataSource = ProposalsRoomDataSource(database)

    @Provides
    fun createProposalsWebDataSource(retrofitService: CouncilApiService): ProposalsWebDataSource = ProposalsWebDataSource(retrofitService)
}