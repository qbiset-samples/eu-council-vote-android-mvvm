package com.enocia.europeancouncilvotes.repositories

import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.ProposalWithCompleteVotes
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class ProposalsRepository @Inject constructor(private val roomDataSource: ProposalsRoomDataSource, private val webDataSource: ProposalsWebDataSource) {

    suspend fun getProposals(forceAPIQuery: Boolean = false): List<Proposal> = coroutineScope {
        if (roomDataSource.databaseIsEmpty() || forceAPIQuery) {
            fetchAllDataFromWebAPIAndSaveIntoDB()
        }

        return@coroutineScope roomDataSource.getProposals()
    }

    suspend fun getDetailedProposal(proposal: Proposal): ProposalWithCompleteVotes = coroutineScope {
        return@coroutineScope roomDataSource.getProposalWithCompleteVotes(proposal)
    }

    private suspend fun fetchAllDataFromWebAPIAndSaveIntoDB() = coroutineScope {
        val countries = async { webDataSource.getCountries() }
        val proposalsWithShallowVotes = async { webDataSource.getProposalsWithShallowVotes() }

        roomDataSource.saveCountries(countries.await())
        roomDataSource.saveProposals(proposalsWithShallowVotes.await())
    }

}