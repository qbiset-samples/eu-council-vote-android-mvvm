package com.enocia.europeancouncilvotes.repositories

import android.annotation.SuppressLint
import com.enocia.europeancouncilvotes.data.CouncilApiService
import com.enocia.europeancouncilvotes.model.Country
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.ProposalWithShallowVotes
import com.enocia.europeancouncilvotes.model.Vote
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import javax.inject.Inject

class ProposalsWebDataSource @Inject constructor(private val retrofitService: CouncilApiService) {

    @Throws(Exception::class)
    suspend fun getCountries(): List<Country> {
        try {
            val countriesJsonString = retrofitService.getCountries()
            return parseCountries(countriesJsonString)
        } catch (e: Exception) {
            throw e
        }
    }

    @Throws(Exception::class)
    suspend fun getProposalsWithShallowVotes(): List<ProposalWithShallowVotes> {
        try {
            val proposalsJsonString = retrofitService.getProposals()
            return parseProposals(proposalsJsonString)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun parseCountries(jsonString: String): List<Country> {
        try {
            val rootObject = JSONObject(jsonString)
            val iterator = rootObject.keys()

            val countries = ArrayList<Country>(rootObject.length())

            while (iterator.hasNext()) {
                val key = iterator.next()
                val country = parseCountry(rootObject, key)

                if (country != null) {
                    countries.add(country)
                }
            }

            return countries
        } catch (e: Exception) {
            throw Exception("Failed to parse countries from JSON.", e)
        }
    }

    private fun parseCountry(countriesJSONObject: JSONObject, key: String): Country? {
        try {
            val countryObject = countriesJSONObject.getJSONObject(key)

            val identifier = countryObject.getString("country_id")
            val name = countryObject.getString("country")
            return Country(identifier, name)
        } catch (e: Exception) {
            println(e.toString())
        }

        return null
    }

    @Throws(Exception::class)
    private fun parseProposals(jsonString: String): List<ProposalWithShallowVotes> {
        try {
            val rootObject = JSONObject(jsonString)
            val rootIterator = rootObject.keys()

            val proposals = ArrayList<ProposalWithShallowVotes>(rootObject.length())

            while (rootIterator.hasNext()) {
                val key = rootIterator.next()
                val proposalWithVotes = parseProposal(rootObject, key)

                if (proposalWithVotes != null) {
                    proposals.add(proposalWithVotes)
                }
            }

            return proposals
        } catch (e: Exception) {
            throw Exception("Failed to parse proposals from JSON.", e)
        }
    }

    @SuppressLint("SimpleDateFormat") // We're trying to parse a yyyy-MM-dd timestamp, no need to localize the date formatter.
    private fun parseProposal(proposalsJSONObject: JSONObject, key: String): ProposalWithShallowVotes? {
        try {
            // Preparing Proposal object
            val proposalObject = proposalsJSONObject.getJSONObject(key)

            val identifier = proposalObject.getString("vote_id")
            val title = proposalObject.getString("vote_title").trim().trimIndent()

            val dateString = proposalObject.getString("date")
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd")
            val date = dateFormatter.parse(dateString)
                ?: throw TypeCastException("Failed to create Date from proposal's timestamp.")

            val proposal = Proposal(identifier, title, date)

            // Preparing the list of associated Vote objects
            val jsonVotes = proposalObject.getJSONArray("votes")
            val votes = parseVotes(jsonVotes)

            // Consolidating everything into the relevant POJO and adding to the list
            return ProposalWithShallowVotes(proposal, votes)
        } catch (e: Exception) {
            println(e.toString())
        }

        return null
    }

    private fun parseVotes(jsonVotes: JSONArray): List<Vote> {

        val votes = ArrayList<Vote>(jsonVotes.length())

        for (index in 0 until jsonVotes.length()) {
            try {
                // Parsing all needed data from JSON string
                val voteObject = jsonVotes.getJSONObject(index)

                val proposalIdentifier: String = voteObject.getString("vote_id")
                val countryIdentifier = voteObject.getString("country_id")
                val voteValue = voteObject.getString("vote")

                // Consolidating all useful data into Vote entity and adding to the list
                val vote = Vote(proposalIdentifier, countryIdentifier, voteValue)
                votes.add(vote)
            } catch (e: Exception) {
                println(e.toString())
            }
        }

        return votes
    }
}