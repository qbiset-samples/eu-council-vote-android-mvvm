package com.enocia.europeancouncilvotes.repositories

import com.enocia.europeancouncilvotes.data.ProposalsDatabase
import com.enocia.europeancouncilvotes.model.Country
import com.enocia.europeancouncilvotes.model.Proposal
import com.enocia.europeancouncilvotes.model.ProposalWithCompleteVotes
import com.enocia.europeancouncilvotes.model.ProposalWithShallowVotes
import javax.inject.Inject

class ProposalsRoomDataSource @Inject constructor(private val database: ProposalsDatabase) {

    suspend fun databaseIsEmpty(): Boolean {
        val proposalDao = database.proposalDao()
        val count = proposalDao.count()
        return (count == 0L)
    }

    suspend fun getProposals(): List<Proposal> {
        val proposalDao = database.proposalDao()
        return proposalDao.loadAllProposals()
    }

    suspend fun getProposalWithCompleteVotes(proposal: Proposal): ProposalWithCompleteVotes {
        val proposalsWithCompleteVotesDao = database.proposalDao()
        return proposalsWithCompleteVotesDao.loadProposalWithCompleteVotes(proposal.identifier).sorted()
    }

    suspend fun saveCountries(countries: List<Country>) {
        val countryDao = database.countryDao()
        countryDao.insertAll(countries)
    }

    suspend fun saveProposals(proposals: List<ProposalWithShallowVotes>) {
        val proposalDao = database.proposalDao()
        val voteDao = database.voteDao()

        proposalDao.insertAll(proposals.map { it.proposal })

        for (proposalWithShallowVotes in proposals) {
            voteDao.insertAll(proposalWithShallowVotes.votes)
        }
    }
}