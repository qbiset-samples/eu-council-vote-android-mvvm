package com.enocia.europeancouncilvotes.model

import androidx.room.*
import androidx.room.ForeignKey.CASCADE

@Entity(
    tableName = "votes",
    primaryKeys = ["proposalIdentifier", "countryIdentifier"],
    foreignKeys = [
        ForeignKey(
            entity = Proposal::class,
            parentColumns = ["identifier"],
            childColumns = ["proposalIdentifier"],
            onUpdate = CASCADE,
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = Country::class,
            parentColumns = ["identifier"],
            childColumns = ["countryIdentifier"],
            onUpdate = CASCADE,
            onDelete = CASCADE
        ),
    ],
    indices = [
        Index("proposalIdentifier"),
        Index("countryIdentifier")
    ]
)
data class Vote(
    val proposalIdentifier: String,
    val countryIdentifier: String,
    val value: String
)

data class VoteAndCountry(
    @Embedded val vote: Vote,
    @Relation(
        parentColumn = "countryIdentifier",
        entityColumn = "identifier"
    )
    val country: Country
) : Comparable<VoteAndCountry> {

    override fun compareTo(other: VoteAndCountry): Int {
        return country.name.compareTo(other.country.name)
    }
}