package com.enocia.europeancouncilvotes.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.text.DateFormat
import java.util.*

@Entity(tableName = "proposals")
data class Proposal(
    @PrimaryKey val identifier: String,
    val title: String,
    val date: Date,
) {

    val formattedDate: String
        get() {
            return dateFormatter.format(date)
        }

    companion object Localization {
        var dateFormatter: DateFormat = DateFormat.getDateInstance(DateFormat.LONG)

        fun refreshDateFormatter() {
            dateFormatter = DateFormat.getDateInstance(DateFormat.LONG)
        }
    }
}

data class ProposalWithShallowVotes(
    val proposal: Proposal,
    val votes: List<Vote>
)

data class ProposalWithCompleteVotes(
    @Embedded val proposal: Proposal,
    @Relation(
        parentColumn = "identifier",
        entity = Vote::class,
        entityColumn = "proposalIdentifier"
    )
    val votes: List<VoteAndCountry>
) {
    fun sorted(): ProposalWithCompleteVotes {
        return ProposalWithCompleteVotes(
            proposal,
            votes.sortedWith { vote1, vote2 -> vote1.country.name.compareTo(vote2.country.name) }
        )
    }
}