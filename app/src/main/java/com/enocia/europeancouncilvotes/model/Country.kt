package com.enocia.europeancouncilvotes.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "countries")
data class Country(
    @PrimaryKey val identifier: String,
    val name: String
)